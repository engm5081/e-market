import { Observable } from 'rxjs';
import { NotificationsService } from './../shared/service/notifications.service';
import { ProductsService } from './../shared/service/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from './../shared/interfaces/Product';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mobileproductinfo',
  templateUrl: './mobileproductinfo.component.html',
  styleUrls: ['./mobileproductinfo.component.scss']
})
export class MobileproductinfoComponent implements OnInit {
  product: Observable<Product> | Product;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private prods: ProductsService,
    private notification: NotificationsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      ({id}) => {
        const product$ = this.prods.getProductViaId(id);
        if (product$ instanceof Observable) {
          product$.subscribe(
            ({product}) => this.product = product,
            ({errors}) => {
              this.notification.addNotification(errors);
              this.router.navigateByUrl('/404');
            }
          );
        } else {
          this.product = product$;
        }
      },
      err => {
        this.notification.addNotification(['Page not found']);
        this.router.navigateByUrl('/404');
      }
    );
  }

}
