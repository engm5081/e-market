import { AuthService } from './service/auth.service';
import { Injectable } from '@angular/core';
import { LoadingService } from './service/loading.service';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { map, catchError, finalize } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable()
export class AppInterceptor implements HttpInterceptor {

  constructor(private loading: LoadingService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const configs: any = {
      url: `http://localhost:8080${request.url}`
    };
    const token = localStorage.getItem('token');
    if (token) {
      configs.setHeaders = {
        Authorization: `Bearer ${token}`
      };
    }
    request = request.clone(configs);
    return next.handle(request).pipe(
      map(req => {
        this.loading.isLoading(true);
        return req;
      }),
      catchError(err => {
        this.loading.isLoading(false);
        return throwError(err.error);
      }),
      finalize(() => {
        this.loading.isLoading(false);
      })
    );

  }

}
