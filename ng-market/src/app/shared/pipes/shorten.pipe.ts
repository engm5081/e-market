import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(str: String, len: number): String {
    if (String(str).length > +len) {
      return str.slice(0, len) + '...';
    }
    return str;
  }

}
