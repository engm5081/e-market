import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { User } from '../interfaces/User';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _auth$: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  auth$: Observable<User> = this._auth$.asObservable();

  private _users: {[_id: string]: User} = {};

  constructor(private http: HttpClient) { }

  register(user) {
    return this.http.post<{email: String}>('/user/register', user);
  }

  login(user) {
    return this.http.post<{user: User, token: string, expiresIn: number}>('/user/login', user);
  }

  authUser(user) {
    this._auth$.next(user);
  }

  getUserViaId(id): Observable<{user: User}> | User {
    if (this._users[id]) {
      return this._users[id];
    }
    return this.http.get<{user: User}>(`/user/${id}`).pipe(map(response => {
      this._users[response.user._id] = response.user;
      return response;
    }));
  }

  uploadUserCover(uploadedCover) {
    return this.http.patch<{id: string, cover: String}>('/user/usercover', uploadedCover, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map(response => {
      if (response.type === HttpEventType.Response) {
        const { cover, id } = response.body;
        this._users[id].cover = cover;
      }
      return response;
    }));
  }


  uploadUserImage(uploadedImage) {
    return this.http.patch<{id: string, image: String}>('/user/userprofileimg', uploadedImage, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map(response => {
      if (response.type === HttpEventType.Response) {
        const { image, id } = response.body;
        this._users[id].image = image;
      }
      return response;
    }));
  }

}
