import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  private _notifications: String[] = [];
  notifications$: BehaviorSubject<String[]> = new BehaviorSubject<String[]>(this._notifications);

  addNotification(notifications: String[]): void {
    if (!notifications) {
      return;
    }
    this._notifications = [
      ...this._notifications,
      ...notifications
    ];
    this.notifications$.next(this._notifications);
  }

  removeNotification(notificationIndex: number): void {
    if (
        typeof notificationIndex !== 'number' ||
        notificationIndex >= this._notifications.length ||
        isNaN(notificationIndex)) {
      return;
    }
    this._notifications.splice(notificationIndex, 1);
    this.notifications$.next(this._notifications);
  }

}
