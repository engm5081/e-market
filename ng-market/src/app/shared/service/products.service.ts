import { Product } from './../interfaces/Product';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private _products: {[productId: string]: any[]} = {};
  private _count: Number;
  private _infoProduct$: Subject<Product> = new Subject<Product>();
  infoProduct$: Observable<Product> = this._infoProduct$.asObservable();

  private _userProducts: {[_id: string]: Product[]} = {};
  private _product: {[_id: string]: Product} = {};

  constructor(private http: HttpClient) {}

  setInfoProduct(product: Product) {
    this._infoProduct$.next(product);
  }


  getProductViaPageNumber(no): Observable<{count: Number, products: Product[]}> | {count: Number, products: Product[]} {
    if (this._products[no]) {
      return {products: this._products[no], count: this._count};
    }
    return this.http.get<{count: Number, products: Product[]}>(`/products/productspage/${no}`).pipe(
      map(data => {
        this._products[no] = data.products;
        this._count = data.count;
        return data;
      })
    );
  }

  getUserProducts(id): Observable<{products: Product[]}> | Product[] {
    if (this._userProducts[id]) {
      return this._userProducts[id];
    }
    return this.http.get<{products: Product[]}>(`/user/products/${id}`).pipe(map(response => {
      this._userProducts[id] = response.products;
      return response;
    }));
  }

  getProductViaId(id): Observable<{product: Product}> | Product {
    const product = this._product[id];
    if (product) {
      return product;
    }
    return this.http.get<{product: Product}>(`/products/product/${id}`);
  }

}
