import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private _loading$: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  loading$: Observable<Boolean> = this._loading$.asObservable();
  layout: HTMLMainElement;

  constructor() { }

  isLoading(state: Boolean) {
    this._loading$.next(state);
    this.scrollToTop(state);
  }

  scrollToTop(state) {
    if (state && this.layout) {
      this.layout.scrollTop = 0;
    }
  }

}
