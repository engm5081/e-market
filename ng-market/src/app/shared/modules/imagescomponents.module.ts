import { ProductimagesComponent } from './../../products/productinfo/productimages/productimages.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ProductimagesComponent
  ],
  exports: [
    ProductimagesComponent
  ]
})
export class ImagescomponentsModule { }
