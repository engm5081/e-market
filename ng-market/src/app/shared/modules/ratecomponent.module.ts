import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RateComponent } from './../../products/product/rate/rate.component';

import {
  MatTooltipModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatTooltipModule
  ],
  declarations: [
    RateComponent
  ],
  exports: [
    RateComponent
  ]
})
export class RatecomponentModule { }
