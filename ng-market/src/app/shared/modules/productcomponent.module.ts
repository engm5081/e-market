import { ShortenPipe } from '../pipes/shorten.pipe';
import { ProductComponent } from './../../products/product/product.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  MatCardModule,
  MatDividerModule,
  MatDialogModule,
  MatIconModule,
  MatTooltipModule,
  MatButtonModule
} from '@angular/material';
import { RatecomponentModule } from './ratecomponent.module';

const components = [
  MatCardModule,
  MatDividerModule,
  MatDialogModule,
  MatIconModule,
  MatTooltipModule,
  MatButtonModule
];

@NgModule({
  imports: [
    CommonModule,
    ...components,
    RouterModule,
    RatecomponentModule
  ],
  declarations: [
    ProductComponent,
    ShortenPipe
  ],
  exports: [
    ProductComponent
  ]
})
export class ProductcomponentModule { }
