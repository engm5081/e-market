export interface User {
  date: String;
  email: String;
  firstName: String;
  gender: String;
  lastName: String;
  _id: string;
  cover: String;
  image: String;
}

/** @readonly think about remove cart & wishlist from backen user */
