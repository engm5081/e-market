export interface Product {
  amount: number;
  date: String;
  describtion: String;
  images: String[];
  owner: String;
  ownerId: String;
  ownerImg: String;
  price: number;
  rate: number;
  name: String;
  _id: String;
}
