import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../shared/interfaces/User';

@Component({
  selector: 'app-usernavigator',
  templateUrl: './usernavigator.component.html',
  styleUrls: ['./usernavigator.component.scss']
})
export class UsernavigatorComponent implements OnInit {
  @Input() user: User;
  constructor() { }

  ngOnInit() {
  }

}
