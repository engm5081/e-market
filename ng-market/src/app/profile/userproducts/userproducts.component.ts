import { NotificationsService } from './../../shared/service/notifications.service';
import { Product } from './../../shared/interfaces/Product';
import { ProductsService } from './../../shared/service/products.service';
import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userproducts',
  templateUrl: './userproducts.component.html',
  styleUrls: ['./userproducts.component.scss']
})
export class UserproductsComponent implements OnInit {
  products: Product[];
  @Input() set id(id) {
    const products$ = this.prods.getUserProducts(id);
    if (products$ instanceof Observable) {
      products$.subscribe(
        ({products}) => {
          console.table(products);
          this.products = products;
        },
        ({errors}) => {
          this.notificationsService.addNotification(errors);
          this.router.navigate(['/404']);
        }
      );
    } else {
      this.products = products$;
    }
  }

  constructor(
    private prods: ProductsService,
    private router: Router,
    private notificationsService: NotificationsService) { }

  ngOnInit() {
  }

}
