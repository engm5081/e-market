import { NotificationsService } from './../../shared/service/notifications.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/interfaces/User';
import { AuthService } from '../../shared/service/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;
  authUser: Observable<User> = this.auth.auth$;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.route.params.subscribe(
      ({id}) => {
        const user$ = this.auth.getUserViaId(id);
        if (user$ instanceof Observable) {
          user$.subscribe(
            ({user}) => this.user = user,
            ({errors}) => {
              this.notificationsService.addNotification(errors);
              this.router.navigate(['/404']);
            }
          );
        } else {
          this.user = user$;
        }
      },
      ({errors}) => {
        this.notificationsService.addNotification(errors);
        this.router.navigate(['/404']);
      }
    );
  }

}
