import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-imageuploader',
  templateUrl: './imageuploader.component.html',
  styleUrls: ['./imageuploader.component.scss']
})
export class ImageuploaderComponent implements OnInit {
  @Input() diameter: number;
  @Input() value: number;
  constructor() { }

  ngOnInit() {
  }

}
