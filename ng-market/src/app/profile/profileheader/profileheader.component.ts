import { NotificationsService } from './../../shared/service/notifications.service';
import { AuthService } from './../../shared/service/auth.service';
import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../shared/interfaces/User';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-profileheader',
  templateUrl: './profileheader.component.html',
  styleUrls: ['./profileheader.component.scss']
})
export class ProfileheaderComponent implements OnInit {
  @Input() user: User;
  @Input() authUser: User;
  coverPresent: number;
  imagePresent: number;
  constructor(
    private auth: AuthService,
    private notificationsService: NotificationsService) { }

  ngOnInit() {
  }

  uploadCover(e) {
    if (!e) {
      return;
    }
    const uploadedCover = (<HTMLInputElement>e.target).files[0];
    const fd = new FormData();
    fd.append('cover', uploadedCover, uploadedCover.name);
    this.auth.uploadUserCover(fd).subscribe(
      (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          this.coverPresent = Math.floor((event.loaded / event.total) * 100);
        } else if (event.type === HttpEventType.Response) {
          this.coverPresent = null;
          const { cover } = event.body;
          this.user.cover = cover;
        } else {
          this.coverPresent = null;
        }
      },
      ({errors}) => {
        this.notificationsService.addNotification(errors);
        this.coverPresent = null;
      }
    );
  }


  uploadImage(e) {
    if (!e) {
      return;
    }
    const uploadedImage = (<HTMLInputElement>e.target).files[0];
    const fd = new FormData();
    fd.append('image', uploadedImage, uploadedImage.name);
    this.auth.uploadUserImage(fd).subscribe(
      (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          this.imagePresent = Math.floor((event.loaded / event.total) * 100);
        } else if (event.type === HttpEventType.Response) {
          this.imagePresent = null;
          const { image } = event.body;
          this.user.image = image;
        } else {
          this.imagePresent = null;
        }
      },
      ({errors}) => {
        this.notificationsService.addNotification(errors);
        this.imagePresent = null;
      }
    );
  }

}
