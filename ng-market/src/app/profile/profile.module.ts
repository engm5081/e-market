import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { ProfileheaderComponent } from './profileheader/profileheader.component';
import { UserproductsComponent } from './userproducts/userproducts.component';
import { UsernavigatorComponent } from './usernavigator/usernavigator.component';
import {
  MatTabsModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatCardModule
} from '@angular/material';
import { ImageuploaderComponent } from './imageuploader/imageuploader.component';
import { ProductcomponentModule } from '../shared/modules/productcomponent.module';

const profileRoutes: Routes = [
  { path: '', component: ProfileComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(profileRoutes),
    MatTabsModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    ProductcomponentModule
  ],
  declarations: [
    ProfileComponent,
    ProfileheaderComponent,
    UserproductsComponent,
    UsernavigatorComponent,
    ImageuploaderComponent
  ]
})
export class ProfileModule { }
