import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent implements OnInit {
  @ViewChild('f') messageForm: NgForm;
  constructor() { }

  ngOnInit() {
  }

  onSubmit({valid, value}: NgForm) {
    if (valid) {
      console.log(value);
      this.messageForm.resetForm();
    } else {
      this.messageForm.reset(value);
    }
  }

}
