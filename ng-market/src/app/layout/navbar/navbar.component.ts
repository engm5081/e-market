import { Observable } from 'rxjs';
import { AuthService } from './../../shared/service/auth.service';
import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from '../../shared/service/notifications.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {

  constructor(
    private router: Router,
    private auth: AuthService,
    private notificationService: NotificationsService) { }


  @Input() drawer;
  @Input() show;
  @Input() user;
  @Input() handSet;
  @Output() notification: EventEmitter<null> = new EventEmitter<null>();
  notifications: Observable<String[]> = this.notificationService.notifications$;

  ngOnInit() {}

  logout(): void {
    localStorage.removeItem('user');
    this.auth.authUser(null);
    this.router.navigate(['/']);
  }

  openNotification() {
    this.notification.emit();
  }

}
