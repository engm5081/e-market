import { Observable } from 'rxjs';
import { NotificationsService } from './../../shared/service/notifications.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent {
  @Input() open: Boolean;
  @Output() closeNotificaiton: EventEmitter<null> = new EventEmitter<null>();
  notifications: Observable<String[]> = this.notificationsService.notifications$;

  constructor(
    private notificationsService: NotificationsService
    ) { }

  close() {
    this.open = false;
    this.closeNotificaiton.emit();
  }

  removeNotification(i) {
    this.notificationsService.removeNotification(i);
  }

}
