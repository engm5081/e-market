import { AuthService } from './../shared/service/auth.service';
import { User } from './../shared/interfaces/User';
import { LoadingService } from './../shared/service/loading.service';
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  @ViewChild('appLayout') appLayout: ElementRef;
  open: Boolean = false;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private loading: LoadingService,
    private auth: AuthService) {}

  isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 800px)')
    .pipe(
      map(result => result.matches)
    );
  loading$: Observable<Boolean> = this.loading.loading$;
  user$: Observable<User> = this.auth.auth$;

      ngOnInit() {
        this.loading.layout = this.appLayout.nativeElement;
      }



  }
