import { IfAuthGuard } from './shared/guards/if-auth.guard';
import { HomeComponent } from './layout/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IfNotauthGuard } from './shared/guards/if-notauth.guard';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { MobileproductinfoComponent } from './mobileproductinfo/mobileproductinfo.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule',
    canActivate: [IfNotauthGuard],
    canActivateChild: [IfNotauthGuard]
  },
  {
    path: 'products/:no',
    loadChildren: './products/products.module#ProductsModule'
  },
  {
    path: 'product/:id',
    component: MobileproductinfoComponent
  },
  {
    path: 'profile/:id',
    loadChildren: './profile/profile.module#ProfileModule',
    canActivate: [IfAuthGuard],
    canActivateChild: [IfAuthGuard]
  },
  {
    path: '404',
    component: PagenotfoundComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
