import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../shared/service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @ViewChild('f') registerForm: NgForm;
  errors: String[];
  constructor(
    private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  register({valid, value}: NgForm) {
    if (valid) {
      this.auth.register(value)
          .subscribe(
            ({email}) => {
              this.router.navigate(['/auth/login'], {
                queryParams: {email}
              });
            },
            ({errors, user}) => {
              this.errors = errors;
              this.registerForm.setValue(user);
            }
          );
    } else {
      this.registerForm.setValue(value);
    }
  }

}
