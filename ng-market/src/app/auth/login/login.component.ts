import { AuthService } from './../../shared/service/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('f') loginForm: NgForm;
  errors: String[];
  email: String;
  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      ({email}) => {
        if (email) {
          this.email = email;
        }
      });
  }

  login({value, valid}: NgForm) {
    if (valid) {
      this.auth.login(value)
          .subscribe(
            ({user, token, expiresIn}) => {
              localStorage.setItem('user', JSON.stringify(user));
              localStorage.setItem('token', token);
              localStorage.setItem('expiresIn', String(expiresIn));
              this.auth.authUser(user);
              this.router.navigate([`/profile/${user._id}`]);
            },
            ({errors}) => {
              this.errors = errors;
            }
          );
    } else {
      this.loginForm.setValue(value);
    }
  }

}
