import { AuthService } from './shared/service/auth.service';
import { LoadingService } from './shared/service/loading.service';
import { Component, OnInit } from '@angular/core';
import {
  Router,
  RouterEvent,
  NavigationStart,
  NavigationError,
  NavigationCancel,
  NavigationEnd
} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
    private loading: LoadingService,
    private auth: AuthService) {
  }

  ngOnInit() {
    this.router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event);
    });

    const user = localStorage.getItem('user');
    if (user) {
      this.auth.authUser(JSON.parse(user));
    }
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading.isLoading(true);
    }
    if (
      event instanceof NavigationEnd ||
      event instanceof NavigationCancel ||
      event instanceof NavigationError
      ) {
      this.loading.isLoading(false);
    }
  }
}

