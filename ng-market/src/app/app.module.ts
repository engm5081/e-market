import { ImagescomponentsModule } from './shared/modules/imagescomponents.module';
import { RatecomponentModule } from './shared/modules/ratecomponent.module';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppInterceptor } from './shared/app.interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LayoutComponent } from './layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { HomeComponent } from './layout/home/home.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { SidenavlinksComponent } from './layout/sidenavlinks/sidenavlinks.component';
import { LoadingComponent } from './loading/loading.component';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatProgressBarModule,
  MatMenuModule,
  MatBadgeModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatTooltipModule
} from '@angular/material';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { NotificationsComponent } from './layout/notifications/notifications.component';
import { HomeSectionComponent } from './layout/home/homesection/homesection.component';
import { HomeservicesComponent } from './layout/home/homeservices/homeservices.component';
import { ParallaxComponent } from './layout/home/parallax/parallax.component';
import { ContactusComponent } from './layout/home/contactus/contactus.component';
import { AgmCoreModule } from '@agm/core';
import { HomefooterComponent } from './layout/home/homefooter/homefooter.component';
import { MobileproductinfoComponent } from './mobileproductinfo/mobileproductinfo.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HomeComponent,
    NavbarComponent,
    SidenavlinksComponent,
    LoadingComponent,
    PagenotfoundComponent,
    NotificationsComponent,
    HomeSectionComponent,
    HomeservicesComponent,
    ParallaxComponent,
    ContactusComponent,
    HomefooterComponent,
    MobileproductinfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    MatProgressBarModule,
    MatMenuModule,
    MatBadgeModule,
    MatCardModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCZlyjwzNf3hHvYHLs2oMRMxOmmvOuPujk'
    }),
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatTooltipModule,
    RatecomponentModule,
    ImagescomponentsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
