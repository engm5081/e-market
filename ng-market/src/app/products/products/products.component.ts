import { BreakpointObserver } from '@angular/cdk/layout';
import { NotificationsService } from './../../shared/service/notifications.service';
import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../shared/service/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { Product } from '../../shared/interfaces/Product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  private products$: Observable<{count: Number, products: Product[]}> | {count: Number, products: Product[]};
  products: Product[];
  count: Number;
  n: number;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 500px)')
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private prods: ProductsService,
    private route: ActivatedRoute,
    private router: Router,
    private notificationsService: NotificationsService,
    private breakpointObserver: BreakpointObserver) { }


  openModule(i) {
    this.prods.setInfoProduct(this.products[i]);
  }

  ngOnInit() {
    this.route.params.subscribe(
      ({no}) => {
        this.n = +no;
        if (isNaN(this.n)) {
          this.notificationsService.addNotification(['Invalid page number']);
          this.router.navigate(['/404']);
          return;
        }
        this.products$ = this.prods.getProductViaPageNumber(no);
        if (this.products$ instanceof Observable) {
          this.products$.pipe(take(1)).subscribe(
            ({count, products}) => {
              this.products = products;
              this.count = +count;
            },
            ({errors}) => {
              this.notificationsService.addNotification(errors);
              this.router.navigate(['/404']);
            }
          );
        } else {
          this.products = this.products$.products;
          this.count = this.products$.count;
        }
      },
      err => {
        this.router.navigate(['/']);
      }
    );
  }
}
