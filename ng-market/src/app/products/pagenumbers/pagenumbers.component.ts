import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-pagenumbers',
  templateUrl: './pagenumbers.component.html',
  styleUrls: ['./pagenumbers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PagenumbersComponent {

  pagesArray: number[];
  private _count: number;
  private _n: number;
  @Input() set n(n) {
    this._n = +n;
    this.setPagesArray(this._count);
  }
  get n(): number {
    return this._n;
  }
  @Input() set count(count) {
    const _count = Math.ceil(+count / 12);
    this._count = !isNaN(_count) ? _count : null;
    this.setPagesArray(this._count);
  }
  get count(): number {
    return this._count;
  }

  setPagesArray(count) {
    if (!count || count < 3) {
      return;
    }
    if (this.n === 1) {
      this.pagesArray = [0, 1, 2];
    } else if (this.n === count) {
      this.pagesArray = [-2, -1, 0];
    } else {
      this.pagesArray = [-1, 0, 1];
    }
  }

}
