import { Product } from './../../shared/interfaces/Product';
import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent implements OnInit {
  @Input() isHandset: Boolean;
  @Input() product: Product;
  @Input() i: number;
  @Output() openModule: EventEmitter<number> = new EventEmitter<number>();
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  onOpenModule() {
    if (this.isHandset) {
      this.router.navigate([`/product/${this.product._id}`]);
    }
    this.openModule.emit(+this.i);
  }

}
