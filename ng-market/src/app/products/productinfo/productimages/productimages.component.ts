import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-productimages',
  templateUrl: './productimages.component.html',
  styleUrls: ['./productimages.component.scss']
})
export class ProductimagesComponent implements OnInit {
  private _images: String [];
  get images(): String[] {
    return this._images;
  }
  @Input() set images(images: String[]) {
    this._images = images;
    this.selectedImg = images[0];
  }
  selectedImg: String;
  constructor() { }

  ngOnInit() {}

  changeImage(img: String) {
    this.selectedImg = img;
  }

}
