import { ProductsService } from './../../shared/service/products.service';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { Product } from '../../shared/interfaces/Product';

@Component({
  selector: 'app-productinfo',
  template: ``
})
export class ProductinfoComponent implements OnInit, OnDestroy {
  product$: Subscription;
  constructor(
    public dialog: MatDialog,
    private prods: ProductsService) {}

  openDialog(data: Product): void {
    this.dialog.open(ProductDialogOverviewComponent, {
      width: 'calc(100% - 30px)',
      data
    });

  }

  ngOnInit() {
    this.product$ = this.prods.infoProduct$.subscribe(
      product => {
        if (!product) { return; }
        this.openDialog(product);
      });
  }

  ngOnDestroy() {
    this.product$.unsubscribe();
  }

}

@Component({
  // selector: 'app-product-dialog',
  templateUrl: 'product-dialog-overview.html',
  styleUrls: ['./productinfo.component.scss']
})
export class ProductDialogOverviewComponent {
  constructor(public dialogRef: MatDialogRef<ProductDialogOverviewComponent>,
    @Inject(MAT_DIALOG_DATA) public product: Product) {}
}
