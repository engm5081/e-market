import { ProductcomponentModule } from '../shared/modules/productcomponent.module';
import { RatecomponentModule } from '../shared/modules/ratecomponent.module';
import { ImagescomponentsModule } from '../shared/modules/imagescomponents.module';
import { ProductsComponent } from './products/products.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PagenumbersComponent } from './pagenumbers/pagenumbers.component';
import {
  MatTooltipModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatDialogModule
} from '@angular/material';
import { ProductinfoComponent, ProductDialogOverviewComponent } from './productinfo/productinfo.component';

const productsRotues: Routes = [
  { path: '', component: ProductsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(productsRotues),
    MatTooltipModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatDialogModule,
    ProductcomponentModule,
    ImagescomponentsModule,
    RatecomponentModule
  ],
  declarations: [
    ProductsComponent,
    PagenumbersComponent,
    ProductinfoComponent,
    ProductDialogOverviewComponent,
  ],
  entryComponents: [ProductDialogOverviewComponent]
})
export class ProductsModule { }
