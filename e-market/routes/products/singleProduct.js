const mongoose = require("mongoose");
const Product = mongoose.model("product");
const { internalError } = require("../../configs/helpers");

const singleProduct = (req, res, next) => {
    const { productId } = req.params;
    if (!mongoose.Types.ObjectId.isValid(productId)) return res.status(400).json({errors: ["Product was not found yet"]});
    Product.findById(productId, (err, product) => {
        if (err) return internalError(res);
        if (!product) return res.status(400).json({errors: ["Product was not found yet"]});
        res.json({product});
    });
};

module.exports = singleProduct;