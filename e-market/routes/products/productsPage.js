const mongoose = require("mongoose");
const Product = mongoose.model("product");
const { internalError } = require("../../configs/helpers");

const productsPage = (req, res, next) => {
    let { pageNo } = req.params;
    try {
        pageNo = parseInt(pageNo);
        if (pageNo < 0) return res.status(400).json({errors: ["Invalid page number"]});
    } catch(e) {
        return res.status(400).json({errors: ["Invalid page number"]});
    }
    Product.countDocuments({}, (err, count) => {
        if (err || !count) return internalError(res);
        if (count === 0) return res.json({products: [], count});
        let skip = (pageNo - 1) * 12;
        Product.find({}, null, {limit: 12, skip}, (err, products) => {
            if (err || !products) return internalError(res);
            if (products.length === 0) return res.status(400).json({errors: ["Invalid page number"]});
            res.json({products, count});
        });
    });

};

module.exports = productsPage;