const mongoose = require("mongoose");
const User = mongoose.model("user");
const Product = mongoose.model("product");
const { getIdViaToken, internalError, removeFiles } = require("../../configs/helpers");

const newProduct = (req, res, next) => {
    req.checkBody("email", "Invalid data").trim().isEmail();
    req.checkBody("name", "Product name is required").trim().notEmpty();
    req.checkBody("name", "Product name min length is 2").trim().len({min: 2});
    req.checkBody("describtion", "Product describtion is required").trim().notEmpty();
    req.checkBody("describtion", "Product describtion min length is 2").trim().len({min: 100});
    req.checkBody("price", "Product price is required").notEmpty();
    req.checkBody("price", "Product price must be integer number eg. 0, 1").isInt();
    req.checkBody("amount", "Product amount is required").notEmpty();
    req.checkBody("amount", "Product amount must be integer number eg. 1, 2").isInt();   
    let errors = req.validationErrors() || [];
    errors = errors.map(err => {
        return err.msg;
    });
    let { files, body } = req;
    if (+body.amount > 100000) errors.push("Product amount cannot pass 100,000");
    if (+body.price > 10000) errors.push("Product price cannot pass 10,000$");
    if (files.length < 1) errors.push(
        "Invalid files type only allowed types are (jpeg - png - jpg) with max size 5 MB",
        "Product should have at least one image and max 10 images"
    );
    if (errors.length > 0) {
        removeFiles(files);
        return res.status(400).json({errors});
    }

    const id = getIdViaToken(req);
    User.findById(id, "email image firstName lastName", (err, user) => {
        if (err) {
            if (files instanceof Array) {
                removeFiles(files);
            }
            return internalError(res);
        }
        if (!user || user.email !== body.email) {
            removeFiles(files);
            return res.status(400).json({errors: ["Invalid data"]});
        }
        const images = [];
        for (let file of files) {
            images.push(file.path.replace(/^\w+\//, ""));
        }
        new Product({
            name: body.name,
            describtion: body.describtion,
            images,
            price: body.price,
            amount: body.amount,
            owner: user.firstName + " " + user.lastName,
            ownerId: id,
            ownerImg: user.image 
        }).save((err, product) => {
            if (err || !product) return internalError(res);
            try {
                const reset = parseInt(body.no) - files.length;
                if (reset === 0 || isNaN(reset)) throw new Error(null);
                res.json({msg: [`Successfully added new product but ${reset} of product images uploading failed`]});
            } catch(_) {
                res.json({msg: ["Successfully added new product"]});
            }
        });
    });
};

module.exports = newProduct;