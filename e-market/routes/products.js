const router = require("express").Router();
const productsPage = require("./products/productsPage");
const singleProduct = require("./products/singleProduct");
const checkAuth = require("../configs/checkAuth");
const multer = require("../configs/multer");
const newProduct = require("./products/newProduct");


/** @requires no-authorization-required */

/** product page router @fires
 * @type GET*/
router.get("/productspage/:pageNo", productsPage);

/** single product router 
 * @type GET*/
router.get("/product/:productId", singleProduct);

/** @requires authorization-required */
router.use(checkAuth);

/** new product router 
 * @type POST*/
router.post("/newproduct", multer("public/productsImages/").array('images', 10), newProduct);

 /** edit product router 
 * - edit images (remove - add)
 * - edit text
 * - edit describtion
 * @type PATCH*/
// TODO: almost same at the upper one!

/** delete product router 
 * @type DELETE
 * 
 * @readonly should remove element from users cart and wishlist
 * */





module.exports = router;