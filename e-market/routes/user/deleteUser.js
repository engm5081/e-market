const mongoose = require("mongoose");
const User = mongoose.model("user");
const { getIdViaToken, internalError } = require("../../configs/helpers");
const bcrypt = require("bcryptjs");

/* TODO: remove user products and it's images and cover/profile images and 
products from other ppl wishlist & cart & send notification */

const deleteUser = (req, res, next) => {
    req.checkBody("email", "Invalid email adress").trim().isEmail();
    req.checkBody("email", "Email address is required").trim().notEmpty();
    req.checkBody("password", "Password is required").trim().notEmpty();
    req.checkBody("rePassword", "New password is required").trim().notEmpty();
    req.checkBody("password", "Passwords are not matched").equals(req.body.rePassword);
    let errors = req.validationErrors() || [];
    if (errors.length > 0) {
        errors = errors.map(err => {
            return err.msg;
        });
        return res.status(400).json({errors});
    }
    const { email, password } = req.body;
    const id = getIdViaToken(req);
    User.findById(id, (err, user) => {
        if (err) return internalError(res);
        if (!user) return res.status(400).json({errors: ["Invalid data"]});
        if (user.email !== email) return res.status(400).json({errors: ["Email is wrong"]});
        bcrypt.compare(password, user.password, (err2, isMatch) => {
            if (err2) return internalError(res);
            if (!isMatch) return res.status(400).json({errors: ["Password is wrong"]});
            user.delete((err3, isRemoved) => {
                if (err3) return internalError(res);
                res.json({msg: ["Successfully removed user"]});
            });
        });
    });
};

module.exports = deleteUser;