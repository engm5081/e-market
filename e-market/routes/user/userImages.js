const mongoose = require("mongoose");
const User = mongoose.model("user");
const sizeOf = require("image-size");
const { getIdViaToken, removeFile, internalError } = require("../../configs/helpers");

/**
 * @param {number} size 
 * @param {string} place
 */
const userImages = (size, place) => {
    return (req, res, next) => {
        console.log("test");
        const errors = [];
        const { file } = req;
        if (typeof file !== "object") errors.push("Invalid type only allowed types are (jpeg - png - jpg) with max size 5 MB");
        if (errors.length > 0) {
            if (file) {
                if (file.path) removeFile(file.path);
            }
            return res.status(400).json({errors});
        }
        sizeOf(file.path, function (err, dimensions) {
            if (err) {
                removeFile(file.path);
                return internalError(res);
            }
            const { width } = dimensions;
            if (width < size) {
                removeFile(file.path);
                return res.status(400).json({errors: ["Cover min width 850px"]});
            }
            const id = getIdViaToken(req);
            User.findById(id, (err2, user) => {
                if (err2) {
                    removeFile(file.path);
                    return internalError(res);
                }
                if (!user) {
                    removeFile(file.path);
                    return res.status(400).json({errors: ["Invalid data"]});
                }
                const old = user[place];
                user[place] = file.path.replace(/^\w+\//, "");
                user.save((err3, newUser) => {
                    if (err3 || !newUser) {
                        removeFile(file.path);
                        return internalError(res);
                    }
                    if (old) removeFile(`public/${old}`);
                    const data = {id};
                    data[place] = newUser[place];
                    res.json(data);
                });
            });
        });
    };
};

module.exports = userImages;