const mongoose = require("mongoose");
const User = mongoose.model('user');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { jwtSecret } = require("../../configs/config");

/* login-POST */
const login = (req, res, next) => {
    req.checkBody("email", "Invalid email adress").trim().isEmail();
    req.checkBody("email", "Email address is required").trim().notEmpty();
    req.checkBody("password", "Password is required").trim().notEmpty();
    let errors = req.validationErrors() || [];
    if (errors.length > 0) {
        errors = errors.map(err => {
            return err.msg;
        });
        return res.status(400).json({errors});
    }
    const { email, password } = req.body;
    User.findOne({email}, (err, user) => {
        if (err) return internalError(res);
        if (!user) return res.status(400).json({errors: ["Invalid email or password"]});
        bcrypt.compare(password, user.password, (error, isMatch) => {
            if (error) return internalError(res);
            if (!isMatch) return res.status(400).json({errors: ["Invalid email or password"]});
            const token = jwt.sign({_id: user._id, date: new Date()}, jwtSecret, {
                expiresIn: 7200
            });
            res.json({
                user: {
                    _id: user._id,
                    email: user.email,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    // wishList: user.wishList,
                    gender: user.gender,
                    // cart: user.cart,
                    date: user.date
                },
                token,
                expiresIn: new Date().getTime() + 7200 * 1000
            });
        });
    });
};

module.exports = login;