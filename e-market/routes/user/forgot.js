const forgotHandlers = {};
const mongoose = require("mongoose");
const User = mongoose.model('user');
const bcrypt = require("bcryptjs");
const message = require("../../configs/message");
const { internalError, randomCode } = require("../../configs/helpers");

/* forgot-POST */
forgotHandlers.forgot = (req, res, next) => {
    req.checkBody("email", "Invalid email adress").trim().isEmail();
    req.checkBody("email", "Email address is required").trim().notEmpty();
    let errors = req.validationErrors() || [];
    if (errors.length > 0) {
        errors = errors.map(err => {
            return err.msg;
        });
        return res.status(400).json({errors});
    }
    const { email } = req.body;
    User.findOne({email}, (err, user) => {
        if (err) return internalError(res);
        if (!user) return res.status(400).json({errors: ["Invalid email address"]});
        const code = randomCode();
        console.log("user", user);
        user.code = code;
        user.save((err2, newUser) => {
            if (err2 || !newUser) internalError(res);
            console.log("user", newUser);
            message(email, 'Forgot password code', `
                change password code: ${code}
            `, (err3, info) => {
                if (err3 || !info) return internalError(res);
                res.json({msg: [`An email was sent to ${email} includes change password code.`]});
            });
        });
    });
};

/* forgotchangepassword-POST */
forgotHandlers.forgotchangepassword = (req, res, next) => {
    req.checkBody("email", "Invalid email adress").trim().isEmail();
    req.checkBody("email", "Email address is required").trim().notEmpty();
    req.checkBody("newPassword", "Password min length is 6").trim().len({min: 6});
    req.checkBody("newPassword", "Password is required").trim().notEmpty();
    req.checkBody("code", "Invalid code").trim().len({min: 20, max: 20});
    let errors = req.validationErrors() || [];
    if (errors.length > 0) {
        errors = errors.map(err => {
            return err.msg;
        });
        return res.status(400).json({errors});
    }
    const { email, code, newPassword } = req.body;
    User.findOne({email}, (err, user) => {
        if (err) return internalError(res);
        if (!user || user.code !== code || user.code === null) return res.status(400).json({errors: ["Invalid data"]});
        user.password = bcrypt.hashSync(newPassword, 10);
        user.code = null;
        user.save((err, newUser) => {
            if (err || !newUser) return internalError(res);
            res.json({msg: ["Password change successfully"]});
        });
    });
};

module.exports = forgotHandlers;