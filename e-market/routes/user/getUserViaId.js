const mongoose = require("mongoose");
const User = mongoose.model("user");
const { internalError } = require("../../configs/helpers");

const getUserViaId = (req, res, next) => {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).json({errors: ["User not found"]});
    User.findById(id, "date email firstName gender lastName _id cover image", (err, user) => {
        if (err) return internalError(res);
        if (!user) return res.status(400).json({errors: ["User not found"]});
        res.json({
            user
        });
    });
}

module.exports = getUserViaId;