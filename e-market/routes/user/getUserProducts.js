const mongoose = require("mongoose");
const Product = mongoose.model("product");
const { internalError } = require("../../configs/helpers");

const getUserProducts = (req, res, next) => {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).json({errors: ["User not found"]});
    Product.find({ownerId: id}, (err, products) => {
        if (err || !products) return internalError(res);
        res.json({
            products
        });
    });
}

module.exports = getUserProducts;