const mongoose = require("mongoose");
const User = mongoose.model('user');
const bcrypt = require("bcryptjs");

/* register-POST */
const register = (req, res, next) => {
    req.checkBody("firstName", "First name min length is 2").trim().len({min: 2})
    req.checkBody("firstName", "First name is required").trim().notEmpty();
    req.checkBody("lastName", "Last name min length is 2").trim().len({min: 2});
    req.checkBody("lastName", "Last name is required").trim().notEmpty();
    req.checkBody("email", "Invalid email adress").trim().isEmail();
    req.checkBody("email", "Email address is required").trim().notEmpty();
    req.checkBody("password", "Password min length is 6").trim().len({min: 6});
    req.checkBody("password", "Password is required").trim().notEmpty();;
    let errors = req.validationErrors() || [];
    const userData = {...req.body};
    if (errors.length > 0) {
        errors = errors.map(err => {
            return err.msg;
        });
        if (userData.gender !== "male" && userData.gender !== "female") errors.push("Invalid gender");
        return res.status(400).json({ user: userData, errors });
    }
    User.findOne({email: userData.email}, (err, isUser) => {
        if (isUser) return res.status(400).json({
            user: {...userData, email: ""},
            errors: ["User already found"]
        });
        if (err) return internalError(res, {user: userData});
        userData.password = bcrypt.hashSync(userData.password, 10);
        new User(userData)
            .save((err2, newUser) => {
                if (err2 || !newUser) return internalError(res, {user: userData});
                res.json({email: newUser.email, msg: ["Successfully register new account"]});
            });
    });
};

module.exports = register;