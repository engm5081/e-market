const mongoose = require("mongoose");
const Product = mongoose.model("product");
const User = mongoose.model("user");
const { getIdViaToken, internalError } = require("../../configs/helpers");
const jwt = require("jsonwebtoken");
const cart = {};

// TODO: try it out

cart.addCart = (req, res, next) => {
    const { productId } = req.params;
    const errors = [];
    if (!mongoose.Types.ObjectId.isValid(productId)) errors.push("Invalid data");
    if (errors.length > 0) return res.json({errors});
    const id = getIdViaToken(req);
    User.findById(id, (err, user) => {
        if (err) return internalError(res);
        if (!user) return res.status(400).json({errors: ["User not found"]});
        Product.findById(productId, (err2, product) => {
            if (err2) return internalError(res);
            if (!product) return  res.statys(400).json({errors: ["Product not found"]});
            if (user.cart.indexOf(productId) > -1) return res.status(400).json({errors: ["Product already in your cart"]});
            if (user.wishList.indexOf(productId) > -1) user.wishList.splice(user.wishList.indexOf(productId), 1);
            user.cart.unshift(productId);
            user.save((err3, newUser) => {
                if (err3 || !newUser) return internalError(res);
                res.json({msg: ["Successfully added product to cart"]});
            });
        });
    });
};

cart.removeCart = (req, res, next) => {
    const { productId } = req.params;
    const errors = [];
    if (!mongoose.Types.ObjectId.isValid(productId)) errors.push("Invalid data");
    if (errors.length > 0) return res.json({errors});
    const id = getIdViaToken(req);
    User.findById(id, (err, user) => {
        if (err) return internalError(res);
        if (!user) return res.status(400).json({errors: ["User not found"]});
        Product.findById(productId, (err2, product) => {
            if (err2) return internalError(res);
            if (!product) return  res.statys(400).json({errors: ["Product not found"]});
            if (user.cart.indexOf(productId) === -1) return res.status(400).json({errors: ["Product is not in your cart"]});
            user.cart.splice(user.cart.indexOf(productId), 1);
            user.save((err3, newUser) => {
                if (err3 || !newUser) return internalError(res);
                res.json({msg: ["Successfully removed product from your cart"]});
            });
        });
    });
};

cart.getCart = (req, res, next) => {
    const id = getIdViaToken(req);
    User.findById(id, (err, user) => {
        if (err) return internalError(err);
        if (!user) return res.status(400).json({errors: ["Invalid data"]});
        Product.find({_id: {$in: user.cart}}, (err2, products) => {
            if (err2) return internalError(err);
            if (!products) return res.json({cart: []});
            res.json({cart: products});
        });
    });
};

module.exports = cart;



