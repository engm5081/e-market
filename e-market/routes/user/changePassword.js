const mongoose = require("mongoose");
const { getIdViaToken, internalError } = require("../../configs/helpers");
const User = mongoose.model("user");
const bcrypt = require('bcryptjs');

const changePassword = (req, res, next) => {
    req.checkBody("email", "Invalid email adress").trim().isEmail();
    req.checkBody("email", "Email address is required").trim().notEmpty();
    req.checkBody("password", "Password is required").trim().notEmpty();
    req.checkBody("newPassword", "New password is required").trim().notEmpty();
    req.checkBody("newPassword", "New password min length is 6").trim().len({min: 6});
    let errors = req.validationErrors() || [];
    if (errors.length > 0) {
        errors = errors.map(err => {
            return err.msg;
        });
        return res.status(400).json({errors});
    }
    const id = getIdViaToken(req);
    const { email, password, newPassword } = req.body;
    User.findById(id, (err, user) => {
        if (err) return internalError(res);
        if (!user) return res.status(400).json({errors: ["Invalid data"]});
        if (user.email !== email) return res.status(400).json({errors: ["Email is wrong"]});
        bcrypt.compare(password, user.password, (err2, isMatch) => {
            if (err2) return internalError(res);
            if (!isMatch) return res.status(400).json({errors: ["Password is wrong"]});
            user.password = bcrypt.hashSync(newPassword);
            user.save((err3, newUser) => {
                if (err3 || !newUser) return internalError(res);
                res.json({msg: ["Password change successfully"]});
            });
        });
    });
};

module.exports = changePassword;