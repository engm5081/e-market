const router = require("express").Router();
const checkAuth = require("../configs/checkAuth");
const register = require("./user/register");
const login = require("./user/login");
const { forgot, forgotchangepassword } = require("./user/forgot");
const changePassword = require("./user/changePassword");
const multer = require("../configs/multer");
const deleteUser = require("./user/deleteUser");
const userImages = require("./user/userImages");
const { addCart, removeCart, getCart } = require("./user/cart");
const { addWishlist, removeWishlist, getWishlist } = require("./user/wishList");
const checkout = require("./user/checkout");
const getUserViaId = require("./user/getUserViaId");
const getUserProducts = require("./user/getUserProducts");


/** @requires no-authorization-required */

/** getUser router @fires
 * @type get*/
router.get("/:id", getUserViaId);

/** getUserProuducts router @fires
 * @type get*/
router.get("/products/:id", getUserProducts);

/** register router @fires
 * @type POST*/
router.post("/register", register);

/** login router @fires
 * @type POST*/
router.post("/login", login);

/** forgot password router 
 * @type POST*/
router.post("/forgot", forgot);

/** change password via forgot password code router
 * @type PATH*/
router.patch("/forgotchangepassword", forgotchangepassword);


/** @requires authorization-required */
router.use(checkAuth);

/** change password router 
 * @type PATH*/
router.patch("/changepassword", changePassword);

/** upload cover image router
 * @type PATH*/
router.patch("/usercover", multer("public/usersCoverImgs/").single('cover'), userImages(850, 'cover'));

/** upload profile image router
* @type PATH*/
router.patch("/userprofileimg", multer("public/usersProfileImgs/").single('image'), userImages(200, 'image'));

/** add to cart router
 * @type PATH
 * @readonly product cannot be in user's cart and wishlist at the same time
 * */
router.patch("/cart/add/:productId", addCart);

/** remove from cart router
 * @type DELETE*/
router.delete("/cart/remove/:productId", removeCart);

/** get user-cart router 
 * @type GET*/
router.get("/cart", getCart);

 /** add to wishlist router
 * @type POST
 * @readonly product cannot be int cart and wishlist at the same time
 * */
router.patch("/wishlist/add/:productId", addWishlist);

/** remove from wishlist router
 * @type DELETE*/
router.delete("/wishlist/remove/:productId", removeWishlist);

/** get user-wishlist router 
 * @type GET*/
router.get("/wishlist", getWishlist);

/** checkout cart list router
 * @type POST*/
router.post("/checkout", checkout);


/** delete user router 
 * @type DELETE*/
router.delete("/deleteuser", deleteUser);

module.exports = router;


/** @readonly
 * send request to other users to allowed sending message via gmail or any other mail
 * - block list for requests
 * - white (accepted) users
 * - send notification for (accept / remove / block / email);
 */