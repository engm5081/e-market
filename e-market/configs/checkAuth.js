const jwt = require("jsonwebtoken");
const { jwtSecret } = require("./config");

const checkAuth = (req, res, next) => {
    try {
        const token = req.headers['authorization'].split(" ")[1];
        jwt.verify(token, jwtSecret);
        next();
    } catch (_) {
        res.status(401).json({
            errors: ["Please login to continue"]
        });
    }
};

module.exports = checkAuth;