const nodemailer = require("nodemailer");
const { nodeMailerAuth } = require("../configs/config");
const message = (to, subject, html, callback) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: nodeMailerAuth
    });
    const mailOptions = {
        from: 'Market ™',
        to,
        subject,
        html
    };
    transporter.sendMail(mailOptions, function (err, info) {
        if (err) return callback(err);
        callback(null, info);
    });
};


module.exports = message;