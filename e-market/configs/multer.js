const multer = require("multer");

const upload = (path) => {
    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, path);
        },
        filename: (req, file, cb) => {
            cb(null, new Date().toISOString() + file.originalname);
        }
    });
    
    const fileFilter = (req, file, cb) => {
        if (
            file.mimetype === 'image/jpeg' ||
            file.mimetype === 'image/png'  ||
            file.mimetype === 'image/jpg') {
                cb(null, true);
            } else {
                cb(null, false);
            }
    };
    
    return multer({
        storage, 
        limits: {
            fieldSize: 1024 * 1024 * 5
        },
        fileFilter
    });
};

module.exports = upload;