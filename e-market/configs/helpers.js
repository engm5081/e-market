const fs = require("fs");
const jwt = require("jsonwebtoken");
const helpers = {};

helpers.removeFile = (path) => {
    fs.unlink(path, (err) => {
        if (err) {}
    });
};

helpers.removeFiles = (files) => {
    for (let file of files) {
        helpers.removeFile(file.path);
    }
};

helpers.internalError = (res, extraData = {}) => {
    return res.status(500).json({
        errors: ["Internal server error please try again!"],
        ...extraData
    });
};


helpers.randomCode = () => {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    let code = '';
    for (let i = 0; i < 20; i++) {
        code += chars[Math.floor(Math.random() * 62)];
    }
    return code;
};

helpers.getIdViaToken = (req) => {
    const token = req.headers['authorization'].split(" ")[1];
    const decode = jwt.decode(token);
    return decode._id;
};

/* helpers.removeDuplicates = (myArr, prop) => {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}
 */
module.exports = helpers;