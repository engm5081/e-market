const express = require("express");
const mongoose = require("mongoose");
const validator = require("express-validator");
const app = express();
const cors = require("cors");
const morgan = require("morgan");
const { port, mongoUrl } = require("./configs/config");

// connect to database
mongoose.connect(mongoUrl, {useNewUrlParser: true}, () => console.log(`DB Connected`));
require("./models/user");
require("./models/product");

// set middle wares
app.use(express.static("public"));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(validator());
app.use(morgan('dev'));
app.use(cors());

// set app routes
const user = require("./routes/user");
const products = require("./routes/products");
app.use("/user", user);
app.use("/products", products);
// TODO: handle any other requests by sending index.html file

app.listen(port, () => console.log(`server started on port: ${port}`));